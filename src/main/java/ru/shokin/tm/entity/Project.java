package ru.shokin.tm.entity;

public class Project {

    /**
     * Уникальный идентификатор проекта
     */

    private Long id = System.nanoTime();

    /**
     * Наименование проекта
     */

    private String name = "";

    /**
     * Информация о проекте
     */

    private String description = "";

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Project id: " + id + ", Project name: " + name + ", Project description: " + description;
    }

}