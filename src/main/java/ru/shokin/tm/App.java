package ru.shokin.tm;

import static ru.shokin.tm.constant.TerminalConst.*;
import static ru.shokin.tm.service.InfoCommand.*;
import static ru.shokin.tm.service.ProjectCommand.*;
import static ru.shokin.tm.service.TaskCommand.*;

import java.util.Scanner;


/**
 * Тестовое приложение для контроля статусов выполняемых задач
 */

public class App {

    final static Scanner scanner = new Scanner(System.in);

    /**
     * Точка входа
     *
     * @param args параметры запуска
     */

    public static void main(final String[] args) {
        run(args);
        displayWelcome();
        process();
    }

    /**
     * Запуск приложения в режиме бесконечного цикла
     */

    private static void process() {
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    /**
     * Запуск приложения с аргументом из командной строки
     *
     * @param args - массив аргументов
     * Для run используется перегрузка методов
     */

    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;

        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * @param command - выполняемая команда
     * @return код ошибки или 0 в случае успешного завершения
     */

    private static int run(final String command) {
        if (command == null || command.isEmpty()) return -1;
        switch (command) {
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return displayExit();

            case PROJECT_CREATE:
                return createProject();
            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_LIST:
                return listProject();
            case PROJECT_VIEW_BY_INDEX:
                return viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return viewProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return removeProjectByIndex();
            case PROJECT_REMOVE_BY_NAME:
                return removeProjectByName();
            case PROJECT_REMOVE_BY_ID:
                return removeProjectById();
            case PROJECT_UPDATE_BY_INDEX:
                return updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return updateProjectById();
            case TASK_CREATE:
                return createTask();
            case TASK_CLEAR:
                return clearTask();
            case TASK_LIST:
                return listTask();
            case TASK_VIEW_BY_INDEX:
                return viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return viewTaskById();
            case TASK_REMOVE_BY_INDEX:
                return removeTaskByIndex();
            case TASK_REMOVE_BY_NAME:
                return removeTaskByName();
            case TASK_REMOVE_BY_ID:
                return removeTaskById();
            case TASK_UPDATE_BY_INDEX:
                return updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return updateTaskById();

            default:
                return displayError();
        }
    }

}