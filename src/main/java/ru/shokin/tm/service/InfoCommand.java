package ru.shokin.tm.service;

public class InfoCommand {

    /**
     * Вывод на экран приветствия
     */

    public static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Вывод на эркан информации о доступных командах
     *
     * @return 0
     */

    public static int displayHelp() {
        System.out.println("*** INFO PANEL ***");
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Closing the application.");
        System.out.println();
        System.out.println("*** PROJECTS ***");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println("project-list  - Display list of projects.");
        System.out.println();
        System.out.println("*** TASKS ***");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        System.out.println("task-list  - Display list of tasks.");

        return 0;
    }

    /**
     * Вывод на экран версии приложения
     *
     * @return 0
     */

    public static int displayVersion() {
        System.out.println("1.0.8");
        return 0;
    }

    /**
     * Вывод на экран сведений о разработчике
     *
     * @return 0
     */

    public static int displayAbout() {
        System.out.println("Developer: Elias Shokin");
        System.out.println("Email: shokin_is@nlmk.com");
        return 0;
    }

    /**
     * Вывод на экран сообщения об ошибке
     *
     * @return -1
     */

    public static int displayError() {
        System.out.println("Error!!! Unknown program arguments...");
        return -1;
    }

    /**
     * Завершение работы приложения
     *
     * @return 0
     */

    public static int displayExit() {
        System.out.println("SEE YOU LATER! :)");
        System.exit(0);
        return 0;
    }

}