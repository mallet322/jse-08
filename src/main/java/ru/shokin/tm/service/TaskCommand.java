package ru.shokin.tm.service;

import ru.shokin.tm.dao.TaskDAO;
import ru.shokin.tm.entity.Task;

import java.util.Scanner;

public class TaskCommand {

    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    /**
     * Создание задания
     *
     * @return 0
     */

    public static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskDAO.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Изменение задания (по индексу, идентификатору)
     *
     * @return 0
     */

    public static int updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskDAO.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskDAO.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public static int updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID:");
        final long id = Long.parseLong(scanner.nextLine());
        final Task task = taskDAO.findById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskDAO.update(id, name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление задания (по индексу, имени, идентификатору)
     *
     * @return 0
     */

    public static int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    public static int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskDAO.removeByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public static int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        final Task task = taskDAO.removeByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public static int removeTaskById() {
        System.out.println("[REMOVE TASK BY ID");
        System.out.println("PLEASE, ENTER TASK ID:");
        final long id = scanner.nextLong();
        final Task task = taskDAO.removeById(id);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Просмотр задачи (по индексу, идентификатору)
     */
    public static void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }


    public static int viewTaskByIndex() {
        System.out.println("PLEASE, ENTER TASK IDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskDAO.findByIndex(index);
        viewTask(task);
        return 0;
    }

    public static int viewTaskById() {
        System.out.println("PLEASE, ENTER TASK ID:");
        final long id = scanner.nextLong();
        final Task task = taskDAO.findById(id);
        viewTask(task);
        return 0;
    }

    /**
     * Список заданий
     *
     * @return 0
     */

    public static int listTask() {
        System.out.println("[LIST TASK]");
        int index = 1;
        for (final Task task : taskDAO.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

}