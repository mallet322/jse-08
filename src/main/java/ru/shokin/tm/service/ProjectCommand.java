package ru.shokin.tm.service;

import ru.shokin.tm.dao.ProjectDAO;
import ru.shokin.tm.entity.Project;

import java.util.Scanner;

public class ProjectCommand {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final Scanner scanner = new Scanner(System.in);

    /**
     * Создание проекта
     *
     * @return 0
     */

    public static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectDAO.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Изменение проекта (по индексу, идентификатору)
     *
     * @return 0
     */

    public static int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectDAO.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectDAO.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public static int updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long id = Long.parseLong(scanner.nextLine());
        final Project project = projectDAO.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectDAO.update(id, name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление проекта (по индексу, имени, идентификатору)
     *
     * @return 0
     */

    public static int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    public static int removeProjectByIndex() {
        System.out.println("REMOVE PROJECT BY ID");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectDAO.removeByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public static int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectDAO.removeByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public static int removeProjectById() {
        System.out.println("REMOVE PROJECT BY ID");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = projectDAO.removeById(id);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Просмотр задачи (по индексу, идентификатору)
     */

    public static void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }


    public static int viewProjectByIndex() {
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectDAO.findByIndex(index);
        viewProject(project);
        return 0;
    }

    public static int viewProjectById() {
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = projectDAO.findById(id);
        viewProject(project);
        return 0;
    }

    /**
     * Список проектов
     *
     * @return 0
     */

    public static int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project : projectDAO.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

}