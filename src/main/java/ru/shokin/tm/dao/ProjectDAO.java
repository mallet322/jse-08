package ru.shokin.tm.dao;

import ru.shokin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectDAO {

    /**
     * Массив с созданными проектами
     */

    private List<Project> projects = new ArrayList<>();

    /**
     * Создание проекта
     * Перегрузка метода для добавления Description
     */

    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    /**
     * Обновление проекта
     */

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    /**
     * Удаление проектов
     * Удаление по индексу, имени , идентификатору
     */

    public void clear() {
        projects.clear();
    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Список созданных проектов
     * Поиск по индексу, имени, идентификатору
     */

    public List<Project> findAll() {
        return projects;
    }

    public Project findByIndex(int index) {
        if (index < 0 || index > projects.size() - 1) {
            System.out.print("THIS PROJECT DOES NOT EXIST!");
            return null;
        }
        return projects.get(index);
    }

    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Project project : projects) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

}